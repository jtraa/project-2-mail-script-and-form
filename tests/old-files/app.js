function ProfanityFilter() {
	
    // The array of bad words START
    var swear_words_arr = new Array(
	"viagra", "buy", "now", "pizza"
    ); 
    
    // The array of bad words END
     
     
    // A regular expression is an object that describes a pattern of characters 
    // We must to find a match at the beginning/end of a word
    // It is different "bad oo" and "badoo"
    // Without "var regex" control, you find a match between "bad oo" and "badoo" because there is "bad" in both sentences
    
    var regex = new RegExp('\\b(' + swear_words_arr.join('|') + ')\\b', 'i' );
  
    // Get form content value
    var alltext = document.querySelector('#message_client').value;
     
    // Bad words controller START
    if(regex.test(alltext)) {
        			
        			// When submit button is clicked, checking for bad words in textarea
                   	$( "#contact-form" ).submit(function( event ) {
				   		if ( $( "textarea" ).val() === "correct" ) {
				   		$( "span" ).text( "Validated..." ).show();
				   		return;
  						}
  					
 
  					$( "span" ).text( "Please note that you can't use swear words" ).css("color", "#ca0201").show().fadeOut(2000);
  						event.preventDefault();
					});
							
    				
         
        // Using – return – the function will stop executing, and return the specified value.
		
		return false;
         }
    else {
	    
	    
	    /* console log */
        console.log("Good Job, No Swear Words");
        } 
        
     // Bad words controller END        
  }
 //-->
 
 
//  $("contact-form").unbind('submit');	
 
 
 
 	//click Submit Button - checks for reCAPTCHA is checked or not
 	$( '#submit-button' ).click(function(event){
 		var $captcha = $( '#recaptcha' ),
 		response = grecaptcha.getResponse();
 		
 		  //input for span classs for when the reCAPTCHA is not checked
		  if (response.length === 0) {
		    $( 'span').text( "reCAPTCHA is mandatory" ).css("color", "#ca0201").show().fadeOut(2000);
		    event.preventDefault();
		    	if( !$captcha.hasClass( "error" ) ){
				$captcha.addClass( "error" );
    	  		}
    	  	
    	  	//deleting span class when the reCAPTCHA is checked
  		   } else {
  		   	$( 'span' ).text('');
  		   		$captcha.removeClass( "error" );
  		   		alert( 'reCAPTCHA marked' );
			}
})
 //-->
 
 


	 
	 
	 
 