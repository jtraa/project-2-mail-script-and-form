<?php

/**
 * Global application settings file
 */
	
//This line tells PHP to display errors
ini_set('display_errors', 1);
//This line displays errors when php starts
ini_set('display_startup_errors', 1);
//This line reports all errors except for notices
error_reporting(E_ALL);


// EOF