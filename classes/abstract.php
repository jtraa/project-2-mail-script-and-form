<?php

namespace Direction;

// Include Settings File
include_once '../config/settings.php';

/**
 * Abstract Redirection Class
 *
 * @author Jelle Traa
 * @global object $post
 * @package Redirection
 */

abstract class Redirection
{
    //Abstract protected and static function getConfirmationPage()
    abstract protected static function getConfirmationPage();
    
}

	
	
// EOF