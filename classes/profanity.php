<?php
	
namespace Validate;

// Include Settings File
include_once '../config/settings.php';

/**
 * Profanity Class
 *
 * @author Jelle Traa
 * @global object $post
 * @package Validate
 */
 class Profanity 
 {
	 	private $usedBadWords = false;
	 		 	
		//Static function for filtering words in the text area
		public function checkWords($message_client) {
				
				//
				$badWords = array('buy', 'now', 'viagra');
				
				foreach ($badWords as $bad_word) {
				
				 	if (strpos($message_client, $bad_word) !== false) {
				 		$this->usedBadWords = true;
            		}	
       
        	}
        	
    	}
		
		public function usedBadWords(){
			
			return $this->usedBadWords;
			
    	}	
 }


//EOF