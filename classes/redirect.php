<?php

namespace Direction;
use \Smarty;

// Include Settings File
include_once '../config/settings.php';

// Including object
include_once 'abstract.php';

/**
 * Redirect Class
 *
 * @author Jelle Traa
 * @global object $post
 * @package Direction
 */

class Redirect extends Redirection
{

    // This is the function which redirects the client to the conformation page
    public static function getConfirmationPage(){
            
            // Get Form Data
            $first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];

            // Include for Smarty class
            include_once '../library/vendor/Smarty/libs/Smarty.class.php';
            
            // Create new object of Smarty
			$smarty = new Smarty;
            
			// Smarty Assign Form data to form after submit
            $smarty->assign('firstname', $first_name); 
            $smarty->assign('lastname', $last_name); 

	    	// This displays the Confirmation Page with smarty display()
            $smarty->display('../views/confirmation-page.php');
    }
    
    // This is the function which redirects the client to the first page
    public static function getIndexPage(){
            

            // Get Form Data
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$email_adress = $_POST['email_adress'];
			$telephone_number = $_POST['telephone_number'];
			$message_client = $_POST['message_client'];
            $subject_mail = $_POST['subject_mail'];
            
            // Include for Smarty class
            include_once '../library/vendor/Smarty/libs/Smarty.class.php';

            // Create new object of Smarty
            $smarty = new Smarty;
            
            // Assign some content which is displayed in the contactform page 
            $smarty->assign('action', '../app/mailing.php'); 
            $smarty->assign('link1', 'https://www.equix.nl');
			
			// Smarty Assign Form data to form after submit
            $smarty->assign('firstname', $first_name); 
            $smarty->assign('lastname', $last_name); 
            $smarty->assign('emailadress', $email_adress); 
            $smarty->assign('telephone', $telephone_number);
            $smarty->assign('subject', $subject_mail);  
            $smarty->assign('message', $message_client); 

	    	// This is a header to the Index Page
            // header('Location: ../index.php');
            $smarty->display('../views/contactform.php');
    }
}


// EOF