<?php

namespace Mail;

// Include Settings File
include_once 'config/settings.php';

/**
 * Interface MailMessage Class
 *
 * @author Jelle Traa
 * @global object $post
 * @package Mail
 */

interface MailMessage
{
	
	// Interface method
	public function sendMail();
	
}
	
	
// EOF