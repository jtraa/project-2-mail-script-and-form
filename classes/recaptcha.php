<?php
	
 namespace Security;
 use Direction\Redirect;

 /**
 * Recaptcha Class
 *
 * @author Jelle Traa
 * @global object $post
 * @package Security
 */
 class Recaptcha
 {
	public function checkRecaptcha($recaptchaResponse){
					
	if (isset($_POST) && !empty($_POST)) {
		$captcha_response = $recaptchaResponse;
		$curl = curl_init();

		$recaptcha_site_secret 	= "6Lfav64UAAAAAKK4OPvO8_17lem9k7qYJwWUU476"; // Populate with your site secret. get it from https://www.google.com/recaptcha/admin		
		$captcha_verify_url = "https://www.google.com/recaptcha/api/siteverify";
		
		curl_setopt($curl, CURLOPT_URL,$captcha_verify_url);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, "secret=".$recaptcha_site_secret."&response=".$captcha_response);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		$captcha_output = curl_exec ($curl);
		curl_close ($curl);
		$decoded_captcha = json_decode($captcha_output);
		$captcha_status = $decoded_captcha->success; // store validation result to a variable.
			

	 	}
		
	}
 }


 // EOF