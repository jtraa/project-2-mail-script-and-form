<?php	

namespace Mail;
use Direction\Redirect;
use \Smarty;
use Validate\Profanity;
use Security\Recaptcha;
use Exception;

// Include Settings File
include_once 'config/settings.php';

// Include Interface Class
include_once 'interface.php';

/**
 * Mail Class
 *
 * @author Jelle Traa
 * @global object $post
 * @package Mail
 */
class Mail implements MailMessage
{		
	// This is for sending the data from the form to the mail
	public function sendMail() {

		// Check For Submit
		if (filter_has_var(INPUT_POST, 'submit')) {
		
			// Get Form Data
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$email_adress = $_POST['email_adress'];
			$telephone_number = $_POST['telephone_number'];
			$message_client = $_POST['message_client'];
			$subject_mail = $_POST['subject_mail'];
			$recaptchaResponse = $_POST['g-recaptcha-response'];
			
				
			// Check words in message from client
			include_once 'profanity.php';
			$checkword = new Profanity();
			$checkword->checkWords($message_client);
					
				// Check if bad words where used
				if ($checkword->usedBadWords()) {
					
					// Give error message to client
					echo '<center><div class="alert alert-warning">Please do not use bad words!</div></center>';
					
					// Redirecting to the Index Page:
					include_once 'redirect.php';
					$location = Redirect::getIndexPage();
					
				} else {
					
					// Google Recaptcha Secret key
					$recaptcha_site_secret 	= "6Lfav64UAAAAAKK4OPvO8_17lem9k7qYJwWUU476"; 
					
					// Checking recaptcha
					if (isset($_POST) && !empty($_POST)) {
						$captcha_response = $recaptchaResponse;

						// Create Curl
						$curl = curl_init();
						
						// Part of the Recaptcha url
						$captcha_verify_url = "https://www.google.com/recaptcha/api/siteverify";
						
						// Settings for Curl
						curl_setopt($curl, CURLOPT_URL,$captcha_verify_url);
						curl_setopt($curl, CURLOPT_POST, true);
						curl_setopt($curl, CURLOPT_POSTFIELDS, "secret=".$recaptcha_site_secret."&response=".$captcha_response);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						
						// Execute Curl
						$captcha_output = curl_exec ($curl);

						// Close Curl
						curl_close ($curl);

						// Decode JSON
						$decoded_captcha = json_decode($captcha_output);
						$captcha_status = $decoded_captcha->success; 
						
						// If statement checking if recaptcha status is false
						if ($captcha_status === FALSE){
							echo '<center><div class="alert alert-warning">The ReCaptcha is invalid. Please try again.</div></center>';

							// Redirecting to the Index Page:
							include_once 'redirect.php';
							$location = Redirect::getIndexPage();
							
							// Return if the recaptcha is invalid
							return;
						}

							// Check Required Fields
							if (!empty($email_adress) && !empty($first_name) && !empty($last_name) && !empty($telephone_number) && !empty($message_client) && !empty($subject_mail)) {
							
								// Passed // Check Email
								if (filter_var($email_adress, FILTER_VALIDATE_EMAIL) === false) {
					
										// When mail failed
										echo '<center><div class="alert alert-warning">Failed: Mail not sent</div></center>';
								
								} else {
					
										// Recipient, Subject and From email defined
										$recipient = 'jelletraa@equix.nl';
										$subject = $subject_mail;
										$from = $email_adress;
										
										// To send HTML mail, the Content-type header must be set
										$headers  = 'MIME-Version: 1.0' . "\r\n";
										$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
										
										// Create email headers
										$headers .= 'From: '.$from."\r\n".
											'Reply-To: '.$from."\r\n" .
											'X-Mailer: PHP/' . phpversion();
						
										// Include for Smarty class
										include '../library/vendor/Smarty/libs/Smarty.class.php';
						
										// Create new object of Smarty
										$smarty = new Smarty;

										// Assign some content which is displayed in the mail template 
										$smarty->assign('from', $from); 
										$smarty->assign('firstname', $first_name); 
										$smarty->assign('lastname', $last_name); 
										$smarty->assign('telephone', $telephone_number);
										$smarty->assign('subject', $subject);  
										$smarty->assign('message', $message_client); 
										
											// Mail is the function which sends all the data to the email (recipient)	
											if(mail($recipient, $subject, $smarty->fetch('../views/mail-template.php'), $headers)) {
							
													// Redirecting to the confirmation page:
													include_once 'redirect.php';
													$location = Redirect::getConfirmationPage();	 			
												
											} else {
													
													// When mail failed
													echo '<center><div class="alert alert-warning">Failed: Mail not sent</div></center>';
											}
								}
							
										
							} else {
								
								// New Exception
								throw new Exception('fields cant be empty');
								
								// Failed
								echo '<center><div class="alert alert-warning">Failed: Fields not empty</div></center>';
							}
						}
						
		 		}
			}
 	}
}


// EOF