<!DOCTYPE html>
<html>
  <body>

    <!-- Container -->
    <div class="container">

                <!-- Title -->
  				<h1 style="color: DarkBlue;">New Message!</h1>

                <!-- Table with smarty components -->
				<table border='1' width="50%">
                    <tr> <td>From:</td> <td>{$from}</td> </tr>
                    <tr> <td>First Name:</td> <td>{$firstname}</td> </tr>
                    <tr> <td>Last Name:</td> <td>{$lastname}</td> </tr>
                    <tr> <td>Telephone:</td> <td>{$telephone}</td> </tr>
                    <tr> <td>Subject:</td> <td>{$subject}</td> </tr>
                    <tr> <td>Message:</td> <td>{$message}</td> </tr>

                <!-- End Table -->
                </table>
                
                <!-- Footer message -->
				<p style="color:Green;font-size:15px;">End Message</p>

    <!-- End Container -->
    </div>
  </body>
</html>