<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/css/style.css">
    <link rel="stylesheet" href="resources/css/style.css">
    <title>Project #2 | Mail Form & Script </title>

    <!-- Font Awesome -->
    <link href="resources/css/fontawesome/fontawesome-pro-5.10.0-11-web/css/all.min.css" rel="stylesheet">
    <link href="../resources/css/fontawesome/fontawesome-pro-5.10.0-11-web/css/all.min.css" rel="stylesheet">

</head>

<body>
   

    <!-- Container -->
    <div class="container">
					
        <!-- Section for the information on top of the page -->
        <section class="pl-3 pt-3 pb-3">

            <h2 class="display-5 font-weight-bold">Contact Us</h2>
            <p class="">Do you have any questions? Please do not hesitate to contact us directly. Our team will come
                back to you within
                a matter of hours to help you.</p>

        <!-- End Section -->
        </section>

        <!--Grid column-->
        <div class="col-md-12 mb-md-0 mb-6">

        
                <!-- Form Action to create.mail.php which sends it to the method sendMail() -->
            <form id="contact-form" name="contact-form" action="{$action}" method="POST">
                
                    <!-- Form Grid - Row -->
                    <div class="form-row">

                       <div class="form-group col-md-3 mr-0">
                            <label for="first_name" class="text-capitalize font-weight-bold">your First name:</label>
                            <input type="text" class="form-control" placeholder="First Name" name="first_name" 
                            value="{$firstname}" required>
                        </div>
                                                    
                        <div class="form-group col-md-3 mr-0">
                            <label for="last_name" class="text-capitalize font-weight-bold">your last name:</label>
                            <input type="text" class="form-control"  Placeholder="Last Name" name="last_name"
                            value="{$lastname}" required>
                        </div> 
                    
                         <div class="form-group col-md-6">
                                <label for="email" class="text-capitalize font-weight-bold">your email:</label>
                                <input type="email" class="form-control" placeholder="Email Adress" name="email_adress"
                                value="{$emailadress}" required>
                        </div>

                     <!-- Ending Form Grid - Row -->
                    </div>


                    <!-- Form Grid - Row -->
                    <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="telephone" class="text-capitalize font-weight-bold">your telephone number:</label>
                                <input type="telephone" class="form-control" placeholder="Telephone" name="telephone_number" 
                                value="{$telephone}" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="subject" class="text-capitalize font-weight-bold">Subject:</label>
                                <input type="text" class="form-control" placeholder="Subject" name="subject_mail"
                                value="{$subject}">
                            </div>

                         <!-- Ending Form Grid - Row -->
                         </div> 

                    <!-- Form Grid - Row -->
                    <div class="form-row">
                         <div class="form-group col-md-12 ml-0">
                            <label for="message" class="text-capitalize font-weight-bold">Message:</label>
                            <textarea type="text" id="message_client" name="message_client" rows="4" 
                                class="form-control" placeholder="Typ Your Message Here.." 
                                 required>{$message}</textarea>
                          </div>

                        <!-- Ending Form Grid - Row -->
                        </div>
                    
                    <!-- Form Grid - Row -->
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <div id="recaptcha" class="g-recaptcha" data-sitekey="6Lfav64UAAAAAAaZ-xQMlIBCD1jhMCLZ5KWw5ZMJ" data-callback="verifyRecaptchaCallback" 
                                data-expired-callback="expiredRecaptchaCallback"></div>
                                <input class="form-control d-none" data-recaptcha="true" data-error="Please complete the Captcha" name="recaptcha">
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group col-md-5 pl-5">
                            <p class="text-center"> Contact Information of the company 
                            at the bottom of this page. Click <a href="{$link1}">here</a> if you want to go to the Equix Design page. </p>
                        </div>
                        <div class="form-group col-md-1"></div>
                            <div class="form-group col-md-2 mt-3">
                                <input id="submit-button" type="submit" value="Send" name="submit" class="btn btn-primary text-white w-100 rounded-0">  
                                <span class="error"></span>                  
						    </div>

                    <!-- Ending Form Grid - Row -->
                    </div>

                    
                    <!-- Grid Column --> <!-- Information about receiver -->
                    <div class="form-row">
                        <div class="form-group col-md-12">
                             <div class="text-center card"> 
                                 <ul class="list-inline">
                                    <br>
                                    <li class="pr-5 list-inline-item"><i class="fas fa-map-marker-alt fa-2x"></i>
                                        <p><hr>Equix, Utrecht 
                                    </li>

                                    <li class="pr-5 list-inline-item"><i class="fas fa-phone-alt mt-4 fa-2x"></i>
                                        <p><hr>+31 624842264
                                    </li>

                                    <li class="list-inline-item"><i class="fas fa-at mt-4 fa-2x"></i>
                                        <p><hr>jelletraa@equix.nl
                                    </li>
                                 </ul>
                             </div>
                        </div>

                    <!-- Ending Form Grid - Row -->
                    </div>
                        
                </form>
            </div>
        

    <!-- Container ending-->
    </div>
        
        <!-- Links jQuery and Bootstrap -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <!-- Recaptcha link -->
        <script src='https://www.google.com/recaptcha/api.js' async defer ></script>

</body>
</html>


<!-- EOF -->