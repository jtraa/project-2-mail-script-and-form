<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
	<!-- links for Bulma CSS -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css.map"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css"/>
	
	<!-- Link to CSS -->
    <link rel="stylesheet" href="../resources/css/style.css">
    <title>Project #2 | Confirmation Page </title>

    <!-- Font Awesome -->
    <link href="resources/css/fontawesome/fontawesome-pro-5.10.0-11-web/css/all.min.css" rel="stylesheet">
</head>
<body>
    <!-- Confirmation Message -->
    <h1 id="thanks" class="thanks title is-1"> Thank you very much {$firstname} {$lastname} !</h1>
    <h4 class="thanks subtitle is-2"> We will respond to your message as soon as possible. </h4>
    
    <br><br>

    <!-- Button Return to Contact Form -->
    <div class="box has-text-centered hero-body">
        <a class="button is-link has-text-weight-bold is-radiusless" href="../index.php">Return To Contact Form</a>
    </div>


</body>
</html>


<!-- EOF -->