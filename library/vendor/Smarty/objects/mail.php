<?php	
namespace mail;
use direction\Redirect;

//Class Mail for all mailing methods
class Mail{


	// Method sendMail() for sending the data from the form to the mail
    public static function sendMail(){
	
	//Check For Submit
	if (filter_has_var(INPUT_POST, 'submit')){
	
		// Get Form Data
		$firstname = $_POST['first_name'];
		$_SESSION['first_name'] = $firstname;
		$lastname = $_POST['last_name'];
		$email = $_POST['email_adress'];
		$telephone = $_POST['telephone_number'];
		$message = $_POST['message_client'];
		$subject = $_POST['subject_mail'];
		$recaptcha = $_POST['recaptcha'];
	
	
		//Check Required Fields
		if (!empty($email) && !empty($firstname) && !empty($lastname) && !empty($telephone) && !empty($message) && !empty($subject)) {
		
			// Passed
			// Check Email
			if(filter_var($email, FILTER_VALIDATE_EMAIL) === false){
			
				alert("something went wrong");
			}else{

			// Content is all the data put together:
			$content = "From: \nFirst Name: " . $firstname . "\nLast Name: " . $lastname . "\nTelephone: " . $telephone . "\nEmail: " . $email . "\nMessage: " . $message;

			// Recipient is the emailadress where the mail is send to:
			$recipient = "jelletraa@equix.nl";
		
			// Mailheader is the email from the message sender
			$mailheader = "From: " . $email . "\r\n" . "CC: log@equix.nl";
			
			// Mail is the function which sends all the data to the email (recipient)	
				if(@mail($recipient, $subject, $content, $mailheader))
				{
		 			echo "Mail Sent Successfully";

					//Redirecting to the confirmation page:
					include_once 'objects/redirect.php';
		 			$location = new Redirect();
		 			$location->getConformationPage();

				}else{
 					echo "Mail Not Sent";
				}
			}				
	} else {
		
		//Failed
		echo 'FAILED';
	}
	
  }
 }

}


// EOF
