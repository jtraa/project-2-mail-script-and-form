<?php

// Include Settings File
include 'config/settings.php';

// Include for Smarty class
include 'library/vendor/Smarty/libs/Smarty.class.php';


// Create new object of Smarty
$smarty = new Smarty;

// Assign some content which is displayed in the contactform page 
$smarty->assign('action', 'app/mailing.php'); 
$smarty->assign('link1', 'https://www.equix.nl');  
 
// Smarty Assign Form data to form after submit
$smarty->assign('firstname', ''); 
$smarty->assign('lastname', ''); 
$smarty->assign('emailadress', ''); 
$smarty->assign('telephone', '');
$smarty->assign('subject', '');  
$smarty->assign('message', ''); 

// Display it
$smarty->display('views/contactform.php');


// EOF