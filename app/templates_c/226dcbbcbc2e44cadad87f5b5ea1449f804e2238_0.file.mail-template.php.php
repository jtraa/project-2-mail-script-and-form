<?php
/* Smarty version 3.1.33, created on 2019-09-12 09:54:49
  from '/home/admin/domains/equix.nl/private_html/development/project-2/views/mail-template.php' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d7a07d9b86403_18074869',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '226dcbbcbc2e44cadad87f5b5ea1449f804e2238' => 
    array (
      0 => '/home/admin/domains/equix.nl/private_html/development/project-2/views/mail-template.php',
      1 => 1568272008,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d7a07d9b86403_18074869 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html>
  <body>

    <!-- Container -->
    <div class="container">

                <!-- Title -->
  				<h1 style="color: DarkBlue;">New Message!</h1>

                <!-- Table with smarty components -->
				<table border='1' width="50%">
                    <tr> <td>From:</td> <td><?php echo $_smarty_tpl->tpl_vars['from']->value;?>
</td> </tr>
                    <tr> <td>First Name:</td> <td><?php echo $_smarty_tpl->tpl_vars['firstname']->value;?>
</td> </tr>
                    <tr> <td>Last Name:</td> <td><?php echo $_smarty_tpl->tpl_vars['lastname']->value;?>
</td> </tr>
                    <tr> <td>Telephone:</td> <td><?php echo $_smarty_tpl->tpl_vars['telephone']->value;?>
</td> </tr>
                    <tr> <td>Subject:</td> <td><?php echo $_smarty_tpl->tpl_vars['subject']->value;?>
</td> </tr>
                    <tr> <td>Message:</td> <td><?php echo $_smarty_tpl->tpl_vars['message']->value;?>
</td> </tr>

                <!-- End Table -->
                </table>
                
                <!-- Footer message -->
				<p style="color:Green;font-size:15px;">End Message</p>

    <!-- End Container -->
    </div>
  </body>
</html><?php }
}
